# Xdact - the ultimate text yeeting toolkit 🚀

Welcome to this dope piece of code where we keep things on the DL.

Ever felt the urge to ghost some words or phrases from your text like you're in stealth mode?

You've hit the jackpot, fam.

Here's the tea:

- Slap '-r' when you boot this bad girl to choose the words you wanna yeet outta your text.
- If those default redaction blocks feel basic, flex with '-w' and switch it up with any character that vibes with you.

Just spill your thoughts or slide in some text, and BAM! Watch as your text transforms, keeping those sus bits on the hush-hush. Ideal for when you wanna spill tea without the mess or clean up your act before sharing that screen in a Zoom sesh.

Stay lit 🔥 and remember, this code keeps your secrets safer than your Finsta.

**Examples**:

  > cat dms.txt | xdact -r "friends_username"

in vim, smoosh out employer name before shaming some bad code in a post.

  > :% !xdact -r "Employer Inc." -w "🧟"

capturing some logs for a bug report

  > journalctl -u broken.service -f | xdact -r "$(cat /etc/hostname)"

really just breaking things

  > dd if=/dev/sda | xdact -r "0" -w "🔥" | dd of=/dev/sda

# FAQ

1. Couldn't I just use sed or awk?

no

2. I'm not sure what I'm doing here.

I'm not sure what I'm doing here.

3. How do I install this?

`$ go install codeberg.org/adasauce/xdact@latest` or just build it and copy it to your PATH.
