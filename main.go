/* Xdact - the ultimate text yeeting toolkit 🚀
 */

package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {
	replacementFlag := flag.String("r", "", "text to redact (required)")
	redactedWithFlag := flag.String("w", "█", "character to use for redaction")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s - the ultimate text yeeting toolkit 🚀\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "This program redacts text from stdin.\n\n")
		fmt.Fprintf(os.Stderr, "Options:\n")

		flag.PrintDefaults()
	}
	flag.Parse()

	if *replacementFlag == "" {
		fmt.Fprintf(os.Stderr, "Error: -r flag is required to replace text. Tell me something ♡ \n\n")
		flag.Usage()
		os.Exit(1)
	}

	redactedText := strings.Repeat(*redactedWithFlag, len(*replacementFlag))

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()

		redacted := strings.ReplaceAll(text, *replacementFlag, redactedText)
		fmt.Println(redacted)
	}

	if err := scanner.Err(); err != nil {
		fmt.Fprintf(os.Stderr, "woops: %v", err)
	}
}
